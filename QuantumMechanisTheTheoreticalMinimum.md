# Quantum Mechanics: The Theoretical Minimum

[Goodreads](https://www.goodreads.com/book/show/18210750-quantum-mechanics)

# Systems and Experiments

In classical physics the apparatus that measures a systems state is usually ignored. That is because its effect on the system is usually insignificant. Doing experiments on quantum level is different. There is no apparatus that is gentle enough to not affect the measured system at this scale. Therefore a measurement always changes the system and you need to take that change into account when describing the state of the system. 

# Quantum States

The first quantum system that is discussed is the spin. It's a property of an elementary particle. An apparatus can read its state as 1 or -1. Depending on the orientation of the apparatus different components of the spin are measured. Measuring in up direction a spin that is already up doesn't change the spin. Although then measuring in left direction will give you a random result. Repeated measurements will average out to zero. We denote this average value with ```<q>``` (Diracs brackets).

So we look at the state of a spin as a vector A in a complex space. These vectors have similar properties to standard vectors but also have some differences. We use the bra- ket notation for complex vectors and their conjugates.

