# What You Do Is Who You Are: How to Create Your Business Culture
### By Ben Horowitz

[ Goodreads ](https://www.goodreads.com/book/show/44063692-what-you-do-is-who-you-are)

## Main Takeaways

* Define a culture by living it. Your decisions, your behaviors will be what your culture becomes
* A leader has to believe in his own values and behave in a way that reflects that
* Reflect on if the rules you are setting are really producing the culture you envision. Look for unwanted side effects and correct them early. 
* Ask your newest employees about how they see your culture. After first week ask:
* * What is different here than in other companies?
* * How would you change it?
* * What is really making you uncomfortable here?
* Sometimes you need to change yourself to change the culture
* Direct contact with your people is extremely important if you want to change the culture.
* Treating outsiders the same as insiders. Apply the same principles.
* Make sure your culture aligns with your personality and your strategy
* Make ethics explicit


## Chapters
### Culture and Revolution
Author looks at leaders of revolutions and how they identified that they had to create a consistent culture in order for the revolution to succeed. He looks at a few different ways how culture is defined, the main insight being: culture is often defined by what you do in difficult situations. An interesting way many leaders changed their culture is by setting an unconventional rule. For example Intel went for the “Casual Dress Code” rule to emphasize that the good ideas should win not nice looks. By enforcing such a rule and repeating the underlying reasons a culture can slowly shift. 

Having set values that you want your people to believe in is not enough. You need define how they should behave in certain instances.

### Toussaint Louverture Applied

### The Way of the Warrior
The Samurai had very strict rules on how to behave. These rules were set very clearly by giving examples. One of the unconventional rules was to always be mindful of death. That way the Samurai were always making sure that if that was their last moment they would still die a honerable death.

By defining very clear behaviors aligning with their values the Samurai always felt an obligation to follow these rules. Basically it is more important to have virtues instead of values. Virtues being behaviors that reflect alignment to the values.

### The Warrior of a Different Way: The Story of Shaka Senghor
White had a difficult childhood and went to prison for murder when he was 19. He spent 20 years in prison. In this chapter he describes how he survived this time and made good use of it. 

In the first week it became clear who is who and how one needs to act (similar in a company). You will be trying to fit in by the second week. White was joining a Squad. A squad has clearly defined values. But his squad leader didn't seem to follow these values which made the squad crumble. -> Lesson: A leader has to believe in his own values and behave in a way that reflects that.

He has overthrown the squad by telling others that the leader is not following the rules. After that he defined rules that fit his own principles. After a few hard decisions he found that these rules were producing unintended side effects. He needed to change the culture. He did that by educating people, having daily meetings that discussed their behavior and their situation. -> Lesson: Direct contact with your people is extremely important when you want to change the culture.

### Shaka Senghor Applied
In a startup you might struggle with getting across a value like: "We need to get the money from our customers". To enforce this value your best bet is to sit down with the whole team every day or even twice a day and ask: "Where is my money?" You need to make clear that this is urgent and we need to behave as group to solve this.

### Genghis Khan, Master of Inclusion
Genghis Khan used a lot of cultural innovation to build his empire based on meritocracy (judging people only on behavior and not on their relationships), loyalty and inclusion. He treated everybody the same not looking at their religion or race. Treating outsiders the same as insiders. 



